# Installs:
# - Java 8
# - Eclipse (python, C, ruby, git)
# - git
# - lxc
# - kvm (libvirt / virtmanager)
# - vagrant
# - terraform
# - chefdk
# - atom
# - docker
# - minukube
# - vncviewer

# References
# https://gist.github.com/xahare/0f2078fc8c52e7ddece1e5ba70c6d5fc
# https://gist.github.com/xahare/0f2078fc8c52e7ddece1e5ba70c6d5fc


# Download Java 8 from Oracle

echo "First download java 8 from oracle from Firefox (Hit ENTER when done)"

echo "Type the path for jdk-(version)-linux-x64.tar.gz"

read JAVA_FIREFOX

sudo apt-get update

sudo apt install qemu-kvm libvirt-clients libvirt-daemon-system   bash-completion debhelper gem2deb libvirt-dev pkg-config   rake libvirt-daemon ebtables dnsmasq libxslt-dev   libxml2-dev libvirt-dev zlib1g-dev ruby-dev virt-manager gdebi chromium git java-package eclipse eclipse-ruby eclipse-egit eclispe-pydev eclipse-cdt gvfs-bin lxc

make-jpkg $JAVA_FIREFOX

sudo dpkg -i oracle-java8-jdk_8u144_amd64.deb

#install javaws on firefox-esr

mkdir .mozilla/plugins/
cd .mozilla/plugins/
ln -s /usr/lib/jvm/oracle-java8-jdk-amd64/jre/lib/amd64/libnpjp2.so


wget https://github.com/atom/atom/releases/download/v1.20.1/atom-amd64.deb

sudo dpkg -i atom-amd64.debo

wget https://www.realvnc.com/download/file/viewer.files/VNC-Viewer-6.17.731-Linux-x64.deb

sudo dpkg -i VNC-Viewer-6.17.731-Linux-x64.deb

curl -fsSL get.docker.com -o get-docker.sh
sh get-docker.sh

sudo usermod -aG docker user

sudo usermod --add-subuids 100000-165536 $USER

wget https://releases.hashicorp.com/terraform/0.10.6/terraform_0.10.6_linux_amd64.zip

unzip terraform_0.10.6_linux_amd64.zip

sudo mv terraform /usr/bin

wget https://releases.hashicorp.com/vagrant/2.0.0/vagrant_2.0.0_x86_64.deb

sudo dpkg -i vagrant_2.0.0_x86_64.deb

wget https://packages.chef.io/files/stable/chefdk/2.3.1/debian/8/chefdk_2.3.1-1_amd64.deb

sudo dpkg -i chefdk_2.3.1-1_amd64.deb

https://packages.chef.io/files/stable/inspec/1.40.0/ubuntu/16.04/inspec_1.40.0-1_amd64.deb

sudo dpkg -i inspec_1.40.0-1_amd64.deb

# kitchen to run with docker
gem install kitchen-docker

# java permissions

#sudo vim /usr/lib/jvm/oracle-java8-jdk-amd64/jre/lib/security/java.policy

#        permission java.lang.RuntimePermission "accessClassInPackage.com.sun.deploy.security";

#        permission java.net.SocketPermission "192.168.19.50:443", "connect,resolve";
