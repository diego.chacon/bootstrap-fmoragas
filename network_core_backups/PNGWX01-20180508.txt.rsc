# may/08/2018 14:34:08 by RouterOS 6.41.3
# software id = ZSAH-F7KZ
#
# model = 951Ui-2HnD
# serial number = 643205CC6E10
/interface bridge
add admin-mac=E4:8D:8C:B9:D0:90 auto-mac=no fast-forward=no name=bridge1-LAN \
    pvid=19
/interface ethernet
set [ find default-name=ether1 ] name=ether1-WAN1
set [ find default-name=ether2 ] name=ether2-WAN2
set [ find default-name=ether3 ] name=ether3-WAN3
set [ find default-name=ether4 ] name=ether4-LAN-SW1
set [ find default-name=ether5 ] name=ether5-LAN-SW2
/interface wireless
set [ find default-name=wlan1 ] disabled=no mode=ap-bridge ssid=fireweb
/interface vrrp
add interface=bridge1-LAN name=vrrp19-LAN priority=254 vrid=19
/interface vlan
add interface=ether1-WAN1 name=vlan14-WAN1 vlan-id=14
add interface=ether2-WAN2 name=vlan15-WAN2 vlan-id=15
add interface=ether3-WAN3 name=vlan16-WAN3 vlan-id=16
add interface=ether4-LAN-SW1 name=vlan19-LAN vlan-id=19
/interface ethernet switch port
set 0 default-vlan-id=14 vlan-header=always-strip
set 1 default-vlan-id=15 vlan-header=always-strip
set 2 default-vlan-id=16 vlan-header=always-strip
set 3 default-vlan-id=19 vlan-header=always-strip
set 4 default-vlan-id=19 vlan-header=always-strip
/interface wireless security-profiles
set [ find default=yes ] authentication-types=wpa2-psk mode=dynamic-keys \
    supplicant-identity=MikroTik wpa2-pre-shared-key=come2internet
/ip pool
add name=dhcp-pool ranges=192.168.19.100-192.168.19.200
/ip dhcp-server
add address-pool=dhcp-pool disabled=no interface=bridge1-LAN name=server1
/interface bridge port
add bridge=bridge1-LAN interface=ether4-LAN-SW1
add bridge=bridge1-LAN hw=no interface=wlan1
add bridge=bridge1-LAN interface=ether5-LAN-SW2
/interface ethernet switch vlan
add ports=ether1-WAN1 switch=switch1 vlan-id=14
add ports=ether2-WAN2 switch=switch1 vlan-id=15
add ports=ether3-WAN3 switch=switch1 vlan-id=16
add ports=ether4-LAN-SW1,ether5-LAN-SW2 switch=switch1 vlan-id=19
/ip address
add address=192.168.19.1/24 interface=ether4-LAN-SW1 network=192.168.19.0
add address=192.168.14.2/24 interface=ether1-WAN1 network=192.168.14.0
add address=192.168.15.2/24 interface=ether2-WAN2 network=192.168.15.0
add address=192.168.16.2/24 interface=ether3-WAN3 network=192.168.16.0
add address=192.168.19.254 interface=vrrp19-LAN network=192.168.19.254
/ip dhcp-server lease
add address=192.168.19.32 comment=PCARM02 mac-address=B8:27:EB:E6:40:19 \
    server=server1
add address=192.168.19.33 comment=PCARM03 mac-address=B8:27:EB:AB:84:F2 \
    server=server1
add address=192.168.19.34 comment=PCARM04 mac-address=B8:27:EB:09:77:1C \
    server=server1
add address=192.168.19.35 comment=PCARM01 mac-address=B8:27:EB:1B:A6:F9 \
    server=server1
add address=192.168.19.30 mac-address=38:60:77:E0:05:E0 server=server1
add address=192.168.19.60 client-id=1:0:25:31:5:da:d1 comment=\
    "PogoPlug Server 1" mac-address=00:25:31:05:DA:D1 server=server1
/ip dhcp-server network
add address=192.168.19.0/24 dns-server=9.9.9.9,149.112.112.112 domain=\
    fmoragas.com gateway=192.168.19.254
/ip dns
set allow-remote-requests=yes cache-size=5000KiB max-udp-packet-size=512 \
    servers=9.9.9.9,149.112.112.112
/ip firewall filter
add action=accept chain=forward dst-port=4365 protocol=udp src-port=4365
/ip firewall mangle
add action=mark-connection chain=input comment=\
    "Mark Input Conection (WAN to ROS)" in-interface=ether1-WAN1 \
    new-connection-mark=ether1-WAN1_conn passthrough=yes
add action=mark-connection chain=input in-interface=ether2-WAN2 \
    new-connection-mark=ether2-WAN2_conn passthrough=yes
add action=mark-routing chain=output comment=\
    "Mark Output Connection (ROS To WAN)" connection-mark=ether1-WAN1_conn \
    new-routing-mark=to_ether1-WAN1 passthrough=yes
add action=mark-routing chain=output connection-mark=ether2-WAN2_conn \
    new-routing-mark=to_ether2-WAN2 passthrough=yes
add action=accept chain=prerouting comment="Accept All prerouting" \
    dst-address=192.168.14.0/24 in-interface=bridge1-LAN
add action=accept chain=prerouting dst-address=192.168.15.0/24 in-interface=\
    bridge1-LAN
add action=accept chain=prerouting dst-address=192.168.16.0/24 in-interface=\
    bridge1-LAN
add action=mark-connection chain=prerouting comment=\
    "Mark Incomming from br1 to WAN1 (6MB) LB Magic" dst-address-type=!local \
    in-interface=bridge1-LAN new-connection-mark=ether1-WAN1_conn \
    passthrough=yes per-connection-classifier=both-addresses-and-ports:7/0
add action=mark-connection chain=prerouting dst-address-type=!local \
    in-interface=bridge1-LAN new-connection-mark=ether1-WAN1_conn \
    passthrough=yes per-connection-classifier=both-addresses-and-ports:7/1
add action=mark-connection chain=prerouting dst-address-type=!local \
    in-interface=bridge1-LAN new-connection-mark=ether1-WAN1_conn \
    passthrough=yes per-connection-classifier=both-addresses-and-ports:7/2
add action=mark-connection chain=prerouting comment=\
    "Mark Incomming from br1 to WAN2 (8MB) LB Magic" dst-address-type=!local \
    in-interface=bridge1-LAN new-connection-mark=ether2-WAN2_conn \
    passthrough=yes per-connection-classifier=both-addresses-and-ports:7/3
add action=mark-connection chain=prerouting dst-address-type=!local \
    in-interface=bridge1-LAN new-connection-mark=ether2-WAN2_conn \
    passthrough=yes per-connection-classifier=both-addresses-and-ports:7/4
add action=mark-connection chain=prerouting dst-address-type=!local \
    in-interface=bridge1-LAN new-connection-mark=ether2-WAN2_conn \
    passthrough=yes per-connection-classifier=both-addresses-and-ports:7/5
add action=mark-connection chain=prerouting dst-address-type=!local \
    in-interface=bridge1-LAN new-connection-mark=ether2-WAN2_conn \
    passthrough=yes per-connection-classifier=both-addresses-and-ports:7/6
add action=mark-routing chain=prerouting comment=\
    "Route Incomming Connection Bridge1 to Some WAN" connection-mark=\
    ether1-WAN1_conn in-interface=bridge1-LAN new-routing-mark=to_ether1-WAN1 \
    passthrough=yes
add action=mark-routing chain=prerouting connection-mark=ether2-WAN2_conn \
    in-interface=bridge1-LAN new-routing-mark=to_ether2-WAN2 passthrough=yes
/ip firewall nat
add action=masquerade chain=srcnat out-interface=ether1-WAN1
add action=masquerade chain=srcnat out-interface=ether2-WAN2
add action=masquerade chain=srcnat out-interface=ether3-WAN3
add action=dst-nat chain=dstnat dst-address=192.168.14.2 dst-port=2222 \
    in-interface=ether1-WAN1 protocol=tcp to-addresses=192.168.19.30 \
    to-ports=22
add action=dst-nat chain=dstnat dst-address=192.168.15.2 dst-port=2222 \
    in-interface=ether2-WAN2 protocol=tcp to-addresses=192.168.19.32 \
    to-ports=22
/ip route
add check-gateway=ping distance=1 gateway=192.168.14.1 routing-mark=\
    to_ether1-WAN1
add check-gateway=ping distance=1 gateway=8.8.8.8 routing-mark=to_ether1-WAN1
add check-gateway=ping distance=1 gateway=192.168.15.1 routing-mark=\
    to_ether2-WAN2
add check-gateway=ping distance=1 gateway=8.8.4.4 routing-mark=to_ether2-WAN2
add check-gateway=ping comment=Default-ISP1 distance=1 gateway=192.168.14.1
add check-gateway=ping comment=Default-ISP2 distance=2 gateway=192.168.15.1
add distance=1 dst-address=8.8.4.4/32 gateway=192.168.15.1 scope=10
add distance=1 dst-address=8.8.8.8/32 gateway=192.168.14.1 scope=10
add comment=bnonline.fi.cr disabled=yes distance=1 dst-address=\
    201.220.29.27/32 gateway=ether1-WAN1
/ip smb
set comment=SMB01 domain=fmoragas.com enabled=yes interfaces=\
    bridge1-LAN,ether4-LAN-SW1,ether5-LAN-SW2,wlan1
/ip smb shares
set [ find default=yes ] disabled=yes
add directory=/disk1 name=isos
/ip smb users
add name=kmoragas password=hitachilg read-only=no
add name=jjmoragag password=hitachilg read-only=no
/system clock
set time-zone-name=America/Costa_Rica
/system identity
set name=PNGWX01
/tool netwatch
add down-script="/ip route set [find comment=\"Default-ISP1\"] disabled=yes" \
    host=8.8.8.8 interval=30s timeout=2s up-script=\
    "/ip route set [find comment=\"Default-ISP1\"] disabled=no"
add down-script="/ip route set [find comment=\"Default-ISP2\"] disabled=yes" \
    host=8.8.4.4 interval=30s timeout=2s up-script=\
    "/ip route set [find comment=\"Default-ISP2\"] disabled=no"
/tool traffic-monitor
add interface=ether1-WAN1 name=tmon1 threshold=0
add interface=ether2-WAN2 name=tmon2 threshold=0
