###########################################################################
# Autor: Kevin Moraga
# email: kmoragas@gmail.com
# fecha: 2020-04-11
#
# Mikrotik Configuracion:
#
#  #####################
#  # 1 # 2 # 3 # 4 # 5 #
#  #####################
#
# Puerto 1: Wan1 - vlan nativa - ECMP
# Puerto 2: Wan2 - vlan nativa - ECMP
# Puerto 3: Wan3 - vlan nativa - Solo para Failover
# Puerto 4: Local - vlan nativa y trunk
# Puerto 5: Local - vlan nativa y trunk
#
# Ademas:
#    - Vlan Guest para Wifi
#    - Vlan OpenVPN para VPN
#    - Configuracion de VRRP en failover para usar los dos routers
#        en los dos ultimos IPs de la red
#
###########################################################################

# Reset router sin configuracion por defecto!!!!!
# /system reset-configuration no-defaults=yes

###############
## Variables ##
###############

:global SysName PNGWX02
:global RouterIp 3

:global Wan1Vlan 14
:global Wan2Vlan 15
:global Wan3Vlan 16
:global LocalVlan 19
:global GuestVlan 2
:global OpenVpnVlan 17

:global DnsPri 1.1.1.1
:global DnsSec 9.9.9.9

:global VrrpPriority 254
:global VrrpIp 254
:global VrrpIpSec 253

:global DhcpStart 150
:global DhcpEnd 199

:global OpenVpnUser kmoragas
:global OpenVpnPass password

#################
## System Name ##
#################

/system identity set name="$SysName"

###################
## VLANs Layer 2 ##
###################

/interface bridge add name=bridge1 vlan-filtering=no

/interface bridge port
add bridge=bridge1 interface=ether1 pvid="$Wan1Vlan"
add bridge=bridge1 interface=ether2 pvid="$Wan2Vlan"
add bridge=bridge1 interface=ether3 pvid="$Wan3Vlan"
add bridge=bridge1 interface=ether4 pvid="$LocalVlan"
add bridge=bridge1 interface=ether5 pvid="$LocalVlan"

/interface bridge vlan
add bridge=bridge1 untagged=ether1 vlan-ids="$Wan1Vlan"
add bridge=bridge1 untagged=ether2 vlan-ids="$Wan2Vlan"
add bridge=bridge1 untagged=ether3 vlan-ids="$Wan3Vlan"
add bridge=bridge1 tagged=ether1,ether2,ether3 untagged=ether4,ether5 vlan-ids="$LocalVlan"

#############################
## Vlan Interfaces Layer 3 ##
#############################

/interface vlan
add name=wan1 vlan-id="$Wan1Vlan" interface=bridge1 disabled=no
add name=wan2 vlan-id="$Wan2Vlan" interface=bridge1 disabled=no
add name=wan3 vlan-id="$Wan3Vlan" interface=bridge1 disabled=no
add name=local vlan-id="$LocalVlan" interface=bridge1 disabled=no

###########################
## configuracion de EMCP ##
###########################

/ ip address
add address="192.168.$LocalVlan.$RouterIp/24" network="192.168.$LocalVlan.0" broadcast="192.168.$LocalVlan.255" interface=local
add address="192.168.$moragasWan1Vlan.$RouterIp/24" network="192.168.$Wan1Vlan.0" broadcast="192.168.$Wan1Vlan.255" interface=wan1
add address="192.168.$Wan2Vlan.$RouterIp/24" network="192.168.$Wan2Vlan.0" broadcast="192.168.$Wan2Vlan.255" interface=wan2

/ ip route
add dst-address="0.0.0.0"/0 gateway="192.168.$Wan1Vlan.1,192.168.$Wan2Vlan.1" check-gateway=ping distance=1 comment="Default ECMP"

/ ip firewall nat 
add chain=srcnat out-interface=wan1 action=masquerade
add chain=srcnat out-interface=wan2 action=masquerade

/ ip firewall mangle
add chain=input in-interface=wan1 action=mark-connection new-connection-mark=wan1_conn
add chain=input in-interface=wan2 action=mark-connection new-connection-mark=wan2_conn   
add chain=output connection-mark=wan1_conn action=mark-routing new-routing-mark=to_wan1     
add chain=output connection-mark=wan2_conn action=mark-routing new-routing-mark=to_wan2     

/ ip route
add dst-address="0.0.0.0"/0 gateway="192.168.$Wan1Vlan.1" routing-mark=to_wan1 check-gateway=ping distance=2 comment="Default Wan1"
add dst-address="0.0.0.0"/0 gateway="192.168.$Wan2Vlan.1" routing-mark=to_wan2 check-gateway=ping distance=2 comment="Default Wan2"

####################################
## Configuracion de Enlace Backup ##
####################################

/ ip address
add address="192.168.$Wan3Vlan.$RouterIp/24" network="192.168.$Wan3Vlan.0" broadcast="192.168.$Wan3Vlan.255" interface=wan3

/ ip firewall nat 
add chain=srcnat out-interface=wan3 action=masquerade

/ ip firewall mangle
add chain=input in-interface=wan3 action=mark-connection new-connection-mark=wan3_conn
add chain=output connection-mark=wan3_conn action=mark-routing new-routing-mark=to_wan3

/ ip route
add dst-address="0.0.0.0"/0 gateway="192.168.$Wan3Vlan.1" routing-mark=to_wan3 check-gateway=ping distance=3 comment="Default Wan3"

########################
## Internet Detection ##
########################

/interface detect-internet set detect-interface-list=all

# check internet detect status

/system script add name="failover_script" source={

:local StatusWan1 ([/interface detect-internet state get wan1]->"state" )
:local StatusWan2 ([/interface detect-internet state get wan2]->"state" )
:local StatusWan3 ([/interface detect-internet state get wan3]->"state" )

:local IpRouteECMP [/ip route find comment="Default ECMP"]
:local IpRouteWan1 [/ip route find comment="Default Wan1"]
:local IpRouteWan2 [/ip route find comment="Default Wan2"]
:local IpRouteWan3 [/ip route find comment="Default Wan3"]

:if ($StatusWan1="internet") do={
	/ip route set $IpRouteWan1 disabled=no
	:log info "Wan1 Up"
} else {
	/ip route set $IpRouteWan1 disabled=yes
	:log info "Wan1 Down"
}

:if ($StatusWan2="internet") do={
	/ip route set $IpRouteWan2 disabled=no
	:log info "Wan2 Up"
} else {
	/ip route set $IpRouteWan2 disabled=yes
	:log info "Wan2 Down"
}

:if ($StatusWan1="internet" && $StatusWan2="internet") do={
	/ip route set $IpRouteECMP disabled=no
	:log info "Both Wans Up"
} else {
	/ip route set $IpRouteECMP disabled=yes
	:log info "Both Wans Down Up"
}

}

/system scheduler add name="failover" on-event="failover_script" interval=30s comment="" disabled=no


################
## VRRP Local ##
################

/interface vrrp add interface=local vrid="$LocalVlan" priority="$VrrpPriority" name="vrrp$LocalVlan" comment="VRRP Primary"
/ip address add address="192.168.$LocalVlan.$VrrpIp/32" interface="vrrp$LocalVlan"

/interface vrrp add interface=local vrid="$VrrpIpSec" name="vrrp$VrrpIpSec" comment="VRRP Secundary"
/ip address add address="192.168.$LocalVlan.$VrrpIpSec/32" interface="vrrp$VrrpIpSec"

###############
## VRRP WANs ##
###############

/interface vrrp add interface=wan1 vrid="$Wan1Vlan" priority="$VrrpPriority" name="vrrp$Wan1Vlan"
/ip address add address="192.168.$Wan1Vlan.$VrrpIp"/32 interface="vrrp$Wan1Vlan"

/interface vrrp add interface=wan2 vrid="$Wan2Vlan" priority="$VrrpPriority" name="vrrp$Wan2Vlan"
/ip address add address="192.168.$Wan2Vlan.$VrrpIp"/32 interface="vrrp$Wan2Vlan"

/interface vrrp add interface=wan3 vrid="$Wan3Vlan" priority="$VrrpPriority" name="vrrp$Wan3Vlan"
/ip address add address="192.168.$Wan3Vlan.$VrrpIp"/32 interface="vrrp$Wan3Vlan"


/interface bridge set bridge1 vlan-filtering=yes

##########
## DHCP ##
##########

/ip pool
add name=dhcp-pool ranges="192.168.$LocalVlan.$DhcpStart-192.168.$LocalVlan.$DhcpEnd"

/ip dhcp-server
add address-pool=dhcp-pool disabled=no interface=local name=local-dhcp-server

/ip dhcp-server network
add address="192.168.$LocalVlan.0"/24 dns-server="1.1.1.1",9.9.9.9 domain=fmoragas.com gateway="192.168.$LocalVlan.254"

/ip dhcp-server lease
add address="192.168.$LocalVlan.35" comment=PCARM05 mac-address=B8:27:EB:40:9E:A9 server=local-dhcp-server
add address="192.168.$LocalVlan.36" comment=PCARM06 mac-address=B8:27:EB:59:0F:E6 server=local-dhcp-server
add address="192.168.$LocalVlan.32" comment=PCARM02 mac-address=B8:27:EB:E6:40:19 server=local-dhcp-server
add address="192.168.$LocalVlan.33" comment=PCARM03 mac-address=B8:27:EB:AB:84:F2 server=local-dhcp-server
add address="192.168.$LocalVlan.34" comment=PCARM04 mac-address=B8:27:EB:09:77:1C server=local-dhcp-server
add address="192.168.$LocalVlan.31" comment=PCARM01 mac-address=B8:27:EB:1B:A6:F9 server=local-dhcp-server
add address="192.168.$LocalVlan.30" comment=PCX8601 mac-address=38:60:77:E0:05:E0 server=local-dhcp-server
add address="192.168.$LocalVlan.60" comment="PogoPlug Server 1" mac-address=00:25:31:05:DA:D1 server=local-dhcp-server

add address="192.168.$LocalVlan.21" comment="PCAM01 - Camara Frontal" mac-address=C4:D6:55:3B:6F:13 server=local-dhcp-server
add address="192.168.$LocalVlan.23" comment="PCAM03 - Frente 2" mac-address=EC:3D:FD:0B:8C:71 server=local-dhcp-server
add address="192.168.$LocalVlan.24" comment="PCAM04 - Atras 2" mac-address=EC:3D:FD:0B:73:01 server=local-dhcp-server
add address="192.168.$LocalVlan.22" comment="Camara Oficina" mac-address=00:62:6E:4A:B4:97 server=local-dhcp-server

add address="192.168.$LocalVlan.11" comment=PNAPU01 mac-address=74:83:C2:02:43:BD
add address="192.168.$LocalVlan.12" comment=PNAPU02 mac-address=18:E8:29:A6:2E:29

add address="192.168.$LocalVlan.15" comment=HDDVR mac-address=00:18:DD:06:8A:F6 server=local-dhcp-server
add address="192.168.$LocalVlan.71" comment="Honeywell Tuxedo" mac-address=B8:2C:A0:3C:B1:9D server=local-dhcp-server
add address="192.168.$LocalVlan.70" comment="Honeywell Alarm" mac-address=B8:2C:A0:65:68:19 server=local-dhcp-server


#########
## DNS ##
#########

/ip dns
set allow-remote-requests=yes cache-size=5000KiB max-udp-packet-size=512 servers="$DnsPri,$DnsSec"

/ip dns static
add address="192.168.$LocalVlan.1" name=PNGWX02

#########
## NAT ##
#########

/ip firewall nat
add action=dst-nat chain=dstnat dst-address="192.168.$Wan1Vlan.$VrrpIp" dst-port=22 in-interface=wan1 protocol=tcp to-addresses="192.168.$LocalVlan.36" to-ports=22
add action=dst-nat chain=dstnat dst-address="192.168.$Wan1Vlan.$VrrpIp" dst-port=32400 in-interface=wan1 protocol=tcp to-addresses="192.168.$LocalVlan.36" to-ports=32400
add action=dst-nat chain=dstnat dst-address="192.168.$Wan1Vlan.$VrrpIp" dst-port=51413 in-interface=wan1 protocol=tcp to-addresses="192.168.$LocalVlan.36" to-ports=51413
add action=dst-nat chain=dstnat dst-address="192.168.$Wan2Vlan.$VrrpIp" dst-port=22 in-interface=wan2 protocol=tcp to-addresses="192.168.$LocalVlan.36" to-ports=22
add action=dst-nat chain=dstnat dst-address="192.168.$Wan2Vlan.$VrrpIp" dst-port=32400 in-interface=wan2 protocol=tcp to-addresses="192.168.$LocalVlan.36" to-ports=32400
add action=dst-nat chain=dstnat dst-address="192.168.$Wan2Vlan.$VrrpIp" dst-port=51413 in-interface=wan2 protocol=tcp to-addresses="192.168.$LocalVlan.36" to-ports=51413

###############
## Guest LAN ##
###############

/interface vlan
add name=guest vlan-id="$GuestVlan" interface=bridge1 disabled=no

/ ip address
add address="192.168.$GuestVlan.$RouterIp/24" network="192.168.$GuestVlan.0" broadcast="192.168.$GuestVlan.255" interface=guest

/interface vrrp add interface=guest vrid="$GuestVlan" priority="$VrrpPriority" name="vrrp$GuestVlan"
/ip address add address="192.168.$GuestVlan.$VrrpIp/32" interface="vrrp$GuestVlan"

/ip pool
add name=guest-pool ranges="192.168.$GuestVlan.$DhcpStart-192.168.$GuestVlan.$DhcpEnd"

/ip dhcp-server
add address-pool=guest-pool interface=guest name=guest-dhcp-server

/ip dhcp-server network
add address="192.168.$GuestVlan.0/24" dns-server="$DnsPri,$DnsSec" domain=fmoragas.com gateway="192.168.$GuestVlan.$VrrpIp"


#############
## OpenVPN ##
#############

/ip pool
add name=ovpn-pool ranges="10.$OpenVpnVlan.32.34"-10.$OpenVpnVlan.32.38

/interface ovpn-server server
set certificate=PNGWX01.crt_0 cipher=blowfish128,aes128,aes192,aes256 default-profile=vpn_profile enabled=\
    yes keepalive-timeout=disabled netmask=29

/ppp profile
add local-address="10.$OpenVpnVlan.32.33" name=vpn_profile remote-address=ovpn-pool use-encryption=require

/ppp secret
add name="$OpenVpnUser" password="OpenVpnPass"

/ip firewall filter
add action=accept chain=forward disabled=yes dst-port=4365 protocol=udp src-port=4365
add action=accept chain=input comment=Openvpn dst-port=1194 protocol=tcp