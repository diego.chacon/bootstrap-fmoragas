import jinja2
import csv

env= jinja2.Environment()
env.loader= jinja2.FileSystemLoader("templates")
template= env.get_template( "xml_template.j2" )

rdr = csv.DictReader( open("TeleCable_Grilla.csv", "r" ) )

print template.render( data=rdr )
