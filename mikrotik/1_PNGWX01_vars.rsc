##################################
# Autor: Kevin Moraga
# email: kmoragas@gmail.com
# fecha: 2020-04-11
##################################

#############
## PNGWX02 ##
#############


# Variables
############

# Variables Especificas de cada Router
:global SysName "PNGWX01"
:global RouterIp 2

:global DhcpStart 100
:global DhcpEnd 149

:global VrrpPriority true