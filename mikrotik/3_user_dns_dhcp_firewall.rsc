#############
# Firewall ##
#############

:global Wan1Vlan 14
:global Wan2Vlan 15
:global Wan3Vlan 16
:global LocalVlan 19
:global GuestVlan 2
:global OpenVpnVlan 17

## NAT

#!!!!!! Rangin Allowed 192.168.X.30-192.168.X.39 by default. Or change $ServerRangeStart and $ServerRangeEnd Variables!!!!!

# Plex
/ip firewall nat add action=dst-nat chain=dstnat connection-mark=incomming_connection_wan1 dst-address="192.168.$Wan1Vlan.$VrrpIp" dst-port=32400 in-interface="vrrp$Wan1Vlan" protocol=tcp to-addresses="192.168.$LocalVlan.35" to-ports=32400
/ip firewall nat add action=dst-nat chain=dstnat connection-mark=incomming_connection_wan2 dst-address="192.168.$Wan2Vlan.$VrrpIp" dst-port=32400 in-interface="vrrp$Wan2Vlan" protocol=tcp to-addresses="192.168.$LocalVlan.35" to-ports=32400

#Torrent
/ip firewall nat add dst-port=51413 protocol=tcp to-addresses="192.168.$LocalVlan.35" to-ports=51413 in-interface="vrrp$Wan1Vlan" action=dst-nat chain=dstnat connection-mark=incomming_connection_wan1 dst-address="192.168.$Wan1Vlan.$VrrpIp"
/ip firewall nat add dst-port=51413 protocol=tcp to-addresses="192.168.$LocalVlan.35" to-ports=51413 in-interface="vrrp$Wan2Vlan" action=dst-nat chain=dstnat connection-mark=incomming_connection_wan1 dst-address="192.168.$Wan2Vlan.$VrrpIp"


#SSH
/ip firewall nat add dst-port=2222 protocol=tcp to-addresses="192.168.$LocalVlan.35" to-ports=22 in-interface="vrrp$Wan1Vlan" action=dst-nat chain=dstnat connection-mark=incomming_connection_wan1 dst-address="192.168.$Wan1Vlan.$VrrpIp"
/ip firewall nat add dst-port=2222 protocol=tcp to-addresses="192.168.$LocalVlan.35" to-ports=22 in-interface="vrrp$Wan2Vlan" action=dst-nat chain=dstnat connection-mark=incomming_connection_wan1 dst-address="192.168.$Wan2Vlan.$VrrpIp"

## Filter

# Torrent
/ip firewall filter add action=accept chain=forward disabled=yes dst-port=4365 protocol=udp src-port=4365




############
## DNS DB ##
############

/ip dns static add address="192.168.$LocalVlan.2" name=PNGWX01.kmoragas.com
/ip dns static add address="192.168.$LocalVlan.3" name=PNGWX02.kmoragas.com
/ip dns static add address="192.168.$LocalVlan.15" name=hdrun.kmoragas.com
/ip dns static add address="192.168.$LocalVlan.35" name=unifi.kmoragas.com
/ip dns static add address="192.168.$LocalVlan.35" name=torrent.kmoragas.com
/ip dns static add address="192.168.$LocalVlan.35" name=plex.kmoragas.com

#############
## DHCP DB ##
#############

# Reserverd IPs

/ip dhcp-server lease add address="192.168.$LocalVlan.35" comment="PCARM05" mac-address=B8:27:EB:40:9E:A9 server=local-dhcp-server
/ip dhcp-server lease add address="192.168.$LocalVlan.36" comment="PCARM06" mac-address=B8:27:EB:59:0F:E6 server=local-dhcp-server
/ip dhcp-server lease add address="192.168.$LocalVlan.32" comment="PCARM02" mac-address=B8:27:EB:E6:40:19 server=local-dhcp-server
/ip dhcp-server lease add address="192.168.$LocalVlan.33" comment="PCARM03" mac-address=B8:27:EB:AB:84:F2 server=local-dhcp-server
/ip dhcp-server lease add address="192.168.$LocalVlan.34" comment="PCARM04" mac-address=B8:27:EB:09:77:1C server=local-dhcp-server
/ip dhcp-server lease add address="192.168.$LocalVlan.31" comment="PCARM01" mac-address=B8:27:EB:1B:A6:F9 server=local-dhcp-server
/ip dhcp-server lease add address="192.168.$LocalVlan.30" comment="PCX8601" mac-address=38:60:77:E0:05:E0 server=local-dhcp-server
/ip dhcp-server lease add address="192.168.$LocalVlan.60" comment="PogoPlug Server 1" mac-address=00:25:31:05:DA:D1 server=local-dhcp-server

/ip dhcp-server lease add address="192.168.$LocalVlan.21" comment="PCAM01 - Camara Frontal" mac-address=C4:D6:55:3B:6F:13 server=local-dhcp-server
/ip dhcp-server lease add address="192.168.$LocalVlan.23" comment="PCAM03 - Frente 2" mac-address=EC:3D:FD:0B:8C:71 server=local-dhcp-server
/ip dhcp-server lease add address="192.168.$LocalVlan.24" comment="PCAM04 - Atras 2" mac-address=EC:3D:FD:0B:73:01 server=local-dhcp-server
/ip dhcp-server lease add address="192.168.$LocalVlan.22" comment="Camara Oficina" mac-address=00:62:6E:4A:B4:97 server=local-dhcp-server

/ip dhcp-server lease add address="192.168.$LocalVlan.11" comment="PNAPU01" mac-address=74:83:C2:02:43:BD server=local-dhcp-server
/ip dhcp-server lease add address="192.168.$LocalVlan.12" comment="PNAPU02" mac-address=18:E8:29:A6:2E:29 server=local-dhcp-server

/ip dhcp-server lease add address="192.168.$LocalVlan.15" comment="HDDVR" mac-address=00:18:DD:06:8A:F6 server=local-dhcp-server
/ip dhcp-server lease add address="192.168.$LocalVlan.71" comment="Honeywell Tuxedo" mac-address=B8:2C:A0:3C:B1:9D server=local-dhcp-server
/ip dhcp-server lease add address="192.168.$LocalVlan.70" comment="Honeywell Alarm" mac-address=B8:2C:A0:65:68:19 server=local-dhcp-server