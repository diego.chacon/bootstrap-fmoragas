##################################
# Autor: Kevin Moraga
# email: kmoragas@gmail.com
# fecha: 2020-04-11
##################################

#############
## PNGWX02 ##
#############


# Variables
############

# Variables Especificas de cada Router
:global SysName "PNGWX02"
:global RouterIp 3

:global DhcpStart 150
:global DhcpEnd 199

:global VrrpPriority false