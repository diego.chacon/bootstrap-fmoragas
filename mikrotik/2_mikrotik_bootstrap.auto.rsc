###########################################################################
# Autor: Kevin Moraga
# email: kmoragas@gmail.com
# fecha: 2020-04-11
#
# Mikrotik Configuracion:
#
#  #####################
#  # 1 # 2 # 3 # 4 # 5 #
#  #####################
#
# Puerto 1: Wan1 - vlan nativa - ECMP
# Puerto 2: Wan2 - vlan nativa - ECMP
# Puerto 3: Wan3 - vlan nativa - Solo para Failover
# Puerto 4: Local - vlan nativa y trunk
# Puerto 5: Local - vlan nativa y trunk
#
# Ademas:
#    - Vlan Guest para Wifi
#    - Vlan OpenVPN para VPN
#    - Configuracion de VRRP en failover para usar los dos routers
#        en los dos ultimos IPs de la red
#    - OpenVPN: Los certificados se llaman igual que el dominio. Es necesario subirlos de primero por FTP
#    - El archivo mikrotik_bootstrap.auto.rsc se ejecuta automaticamente si se sube por FTP (no probado, 
#       necesita input del usuario para el passphrase de los certificados)
#
###########################################################################

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# 1) Reset router sin configuracion por defecto!!!!!
#
# /system reset-configuration no-defaults=yes
#
# Basado en: https://wiki.mikrotik.com/wiki/Manual:Configuration_Management
#
# 2) Subir los certificados con el nombre de dominio.crt, etc
# 
# 3) Subir este archivo, las variables especificas, la configuracion de usuario para firewall, dhcp y dns, y luego ejecutar
#
# /import 1_PNGWX02_vars.rsc verbose=yes
# /import 2_mikrotik_bootstrap.auto.rsc verbose=yes
# /import 3_user_dns_dhcp_firewall.rsc
#
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

###############
## Variables ##
###############

# Variables Especificas de cada Router (ver archivo: 1_PNGWX02_vars.rsc)
#:global SysName "PNGWX02"
#:global RouterIp 3

#:global DhcpStart 150
#:global DhcpEnd 199

#:global VrrpPriority true

# Variables compartidas por los routers
:global Domain "kmoragas.com"
:global Wan1Vlan 14
:global Wan2Vlan 15
:global Wan3Vlan 16
:global LocalVlan 19
:global GuestVlan 2
:global OpenVpnVlan 17

:global VrrpIp 254
:global VrrpIpSec 253

:global DnsPri 1.1.1.1
:global DnsSec 9.9.9.9

:global OpenVpnUser "kmoragas"
:global OpenVpnPass "password"

#################
## System Name ##
#################

/system identity set name="$SysName"

###################
## VLANs Layer 2 ##
###################

# SWITCH VLANS version
##########################

#/interface bridge add name=bridge1

## SWITCH VLANS: add ports to bridge 
#/interface bridge port
#add bridge=bridge1 interface=ether1 hw=yes
#add bridge=bridge1 interface=ether2 hw=yes
#add bridge=bridge1 interface=ether3 hw=yes
#add bridge=bridge1 interface=ether4 hw=yes
#add bridge=bridge1 interface=ether5 hw=yes

# SWITCH VLANS: configure switch ports
#  Note only QCA8337 & Atheros8327 switch chips support hybrid ports, it is not possible to have hardware switched hybrid ports on your RB941-2nD.!!!!!!!!!
#  https://wiki.mikrotik.com/wiki/Manual:Switch_Chip_Features#VLAN_Example_2_.28Trunk_and_Hybrid_Ports.29
#
#/interface ethernet switch port
#set ether1 vlan-mode=secure vlan-header=always-strip default-vlan-id="$Wan1Vlan"
#set ether2 vlan-mode=secure vlan-header=always-strip default-vlan-id="$Wan2Vlan"
#set ether3 vlan-mode=secure vlan-header=always-strip default-vlan-id="$Wan3Vlan"
#set ether4 vlan-mode=secure vlan-header=always-strip default-vlan-id="$LocalVlan"
#set ether5 vlan-mode=secure vlan-header=leave-as-is default-vlan-id="$LocalVlan"
#set switch1-cpu vlan-mode=secure vlan-header=leave-as-is

##set ports in switch
#/interface ethernet switch vlan
#add ports=ether5,switch1-cpu,ether1 switch=switch1 vlan-id="$Wan1Vlan"
#add ports=ether5,switch1-cpu,ether2 switch=switch1 vlan-id="$Wan2Vlan"
#add ports=ether5,switch1-cpu,ether3 switch=switch1 vlan-id="$Wan3Vlan"
##management vlan
#add ports=ether4,ether5,switch1-cpu switch=switch1 vlan-id="$LocalVlan"


# Bridge Version
#################

# Basado en: https://wiki.mikrotik.com/wiki/Manual:Interface/Bridge#Bridge_VLAN_Filtering

#disable vlan-filtering
/interface bridge add name=bridge1 vlan-filtering=no

:if ($VrrpPriority=true) do={
    # STP Root port by default
    # https://wiki.mikrotik.com/wiki/Manual:Spanning_Tree_Protocol
    /interface bridge port
    add bridge=bridge1 interface=ether1 pvid="$Wan1Vlan" path-cost=30
    add bridge=bridge1 interface=ether2 pvid="$Wan2Vlan" path-cost=40
    add bridge=bridge1 interface=ether3 pvid="$Wan3Vlan" path-cost=50
    add bridge=bridge1 interface=ether4 pvid="$LocalVlan" path-cost=10
    add bridge=bridge1 interface=ether5 pvid="$LocalVlan" path-cost=20
} else {
    /interface bridge port
    add bridge=bridge1 interface=ether1 pvid="$Wan1Vlan"
    add bridge=bridge1 interface=ether2 pvid="$Wan2Vlan"
    add bridge=bridge1 interface=ether3 pvid="$Wan3Vlan"
    add bridge=bridge1 interface=ether4 pvid="$LocalVlan"
    add bridge=bridge1 interface=ether5 pvid="$LocalVlan"
}

/interface bridge vlan
add bridge=bridge1 vlan-ids="$Wan1Vlan" untagged=ether1 tagged=ether4,ether5
add bridge=bridge1 vlan-ids="$Wan2Vlan" untagged=ether2 tagged=ether4,ether5
add bridge=bridge1 vlan-ids="$Wan3Vlan" untagged=ether3 tagged=ether4,ether5
add bridge=bridge1 vlan-ids="$LocalVlan" untagged=ether4,ether5

#############################
## Vlan Interfaces Layer 3 ##
#############################

/interface vlan
add name=wan1 vlan-id="$Wan1Vlan" interface=bridge1 disabled=no
add name=wan2 vlan-id="$Wan2Vlan" interface=bridge1 disabled=no
add name=wan3 vlan-id="$Wan3Vlan" interface=bridge1 disabled=no
add name=local vlan-id="$LocalVlan" interface=bridge1 disabled=no

###########################
## configuracion de EMCP ##
###########################

# Basado en: https://wiki.mikrotik.com/wiki/ECMP_load_balancing_with_masquerade

/ip address
add address="192.168.$LocalVlan.$RouterIp/24" network="192.168.$LocalVlan.0" broadcast="192.168.$LocalVlan.255" interface=local
add address="192.168.$Wan1Vlan.$RouterIp/24" network="192.168.$Wan1Vlan.0" broadcast="192.168.$Wan1Vlan.255" interface=wan1
add address="192.168.$Wan2Vlan.$RouterIp/24" network="192.168.$Wan2Vlan.0" broadcast="192.168.$Wan2Vlan.255" interface=wan2

/ip route
add dst-address="0.0.0.0/0" gateway="192.168.$Wan1Vlan.1,192.168.$Wan2Vlan.1" check-gateway=ping distance=1 comment="Default ECMP"

/ip firewall nat 
add chain=srcnat out-interface=wan1 action=masquerade
add chain=srcnat out-interface=wan2 action=masquerade

/ip firewall mangle
add chain=input in-interface=wan1 action=mark-connection new-connection-mark=wan1_conn
add chain=input in-interface=wan2 action=mark-connection new-connection-mark=wan2_conn
add chain=output connection-mark=wan1_conn action=mark-routing new-routing-mark=to_wan1
add chain=output connection-mark=wan2_conn action=mark-routing new-routing-mark=to_wan2

/ip route
add dst-address="0.0.0.0/0" gateway="192.168.$Wan1Vlan.1" routing-mark=to_wan1 distance=1 comment="Default Wan1 Marked"
add dst-address="0.0.0.0/0" gateway="192.168.$Wan2Vlan.1" routing-mark=to_wan2 distance=1 comment="Default Wan2 Marked"

####################################
## Configuracion de Enlace Backup ##
####################################

/ip address
add address="192.168.$Wan3Vlan.$RouterIp/24" network="192.168.$Wan3Vlan.0" broadcast="192.168.$Wan3Vlan.255" interface=wan3

/ip firewall nat 
add chain=srcnat out-interface=wan3 action=masquerade

/ip firewall mangle
add chain=input in-interface=wan3 action=mark-connection new-connection-mark=wan3_conn
add chain=output connection-mark=wan3_conn action=mark-routing new-routing-mark=to_wan3

/ip route
add dst-address="0.0.0.0/0" gateway="192.168.$Wan3Vlan.1" routing-mark=to_wan3 distance=1 comment="Default Wan3 Marked"

# Failover Wan3
/ip route
add dst-address="0.0.0.0/0" gateway="192.168.$Wan3Vlan.1" check-gateway=ping distance=3 comment="Default Wan3 Failover"

# Failover Wan1 and Wan2
/ip route
add dst-address="0.0.0.0/0" gateway="192.168.$Wan1Vlan.1" check-gateway=ping distance=2 comment="Default Wan1 Failover"
add dst-address="0.0.0.0/0" gateway="192.168.$Wan2Vlan.1" check-gateway=ping distance=2 comment="Default Wan2 Failover"

########################
## Internet Detection ##
########################

/interface list add name=wans

/interface list member add list=wans interface=wan1
/interface list member add list=wans interface=wan2
/interface list member add list=wans interface=wan3

/interface detect-internet set detect-interface-list=wans

# check internet detect status script

/system script add name="failover_script" source={

:global Wan1Down
:global Wan2Down
:global ECMPDown
:global Wan3Down

:local StatusWan1 ([/interface detect-internet state get wan1]->"state" )
:local StatusWan2 ([/interface detect-internet state get wan2]->"state" )
:local StatusWan3 ([/interface detect-internet state get wan3]->"state" )

:local IpRouteECMP [/ip route find comment="Default ECMP"]
:local IpRouteWan1 [/ip route find comment="Default Wan1 Failover"]
:local IpRouteWan2 [/ip route find comment="Default Wan2 Failover"]
:local IpRouteWan3 [/ip route find comment="Default Wan3 Failover"]

:local IpRouteWan1Marked [/ip route find comment="Default Wan1 Marked"]
:local IpRouteWan2Marked [/ip route find comment="Default Wan2 Marked"]

# Wan1 is up?
:if ($StatusWan1="internet") do={
    :if ($Wan1Down=true) do={
        /ip route set $IpRouteWan1 disabled=no
        :set Wan1Down false
        :log info "Wan1 Only Up Again"
    }
} else {
    # not true?: for the first run, variable $Wan1Down is not initialized!!!.
    :if ($Wan1Down!=true) do={
        /ip route set $IpRouteWan1 disabled=yes
        :set Wan1Down true
        :log info "Wan1 Only Down"
    }
}

# Wan2 is up?
:if ($StatusWan2="internet") do={
    :if ($Wan2Down=true) do={
        /ip route set $IpRouteWan2 disabled=no
        :set Wan2Down false
        :log info "Wan2 Only Up Again"
    }
} else {
    # not true?: for the first run, variable $Wan2Down is not initialized!!!.
    :if ($Wan2Down!=true) do={
        /ip route set $IpRouteWan2 disabled=yes
        :set Wan2Down true
        :log info "Wan2 Only Down"
    }
}

# Both UP?
:if ($StatusWan1="internet" & $StatusWan2="internet") do={
    :if ($ECMPDown=true) do={
        /ip route set $IpRouteECMP disabled=no
        #/ip route set $IpRouteWan1Marked disabled=no
        #/ip route set $IpRouteWan2Marked disabled=no
        :set ECMPDown false
        :log info "ECMP Up Again"
    }
} else {
    # not true?: for the first run, variable ECMPDown is not initialized!!!.
    :if ($ECMPDown!=true) do={
        /ip route set $IpRouteECMP disabled=yes
        #/ip route set $IpRouteWan1Marked disabled=yes
        #/ip route set $IpRouteWan2Marked disabled=yes
        :set ECMPDown true
        :log info "ECMP Down"
    }
}

:if ($Wan1Down=true & $Wan2Down=true) do={
    :log info "Wan3 Failover Working"
}

}

# setup scheduling job for check
/system scheduler add name="failover" on-event="failover_script" interval=30s comment="" disabled=no


################
## VRRP Local ##
################


## Basado en: https://wiki.mikrotik.com/wiki/Manual:VRRP-examples

/interface vrrp add interface=local vrid="$LocalVlan" priority="100" name="vrrp$LocalVlan" comment="VRRP Primary"
/ip address add address="192.168.$LocalVlan.$VrrpIp/32" interface="vrrp$LocalVlan"

:if ($VrrpPriority=true) do={
    /interface vrrp set [find name="vrrp$LocalVlan"] priority=254
}

/interface vrrp add interface=local vrid="$VrrpIpSec" priority="254" name="vrrp$VrrpIpSec" comment="VRRP Secundary"
/ip address add address="192.168.$LocalVlan.$VrrpIpSec/32" interface="vrrp$VrrpIpSec"

:if ($VrrpPriority=true) do={
    /interface vrrp set [find name="vrrp$VrrpIpSec"] priority=100
}

###############
## VRRP WANs ##
###############

# Creating routing marks for vrrp_wans
/ip firewall mangle
add chain=input in-interface="vrrp$Wan1Vlan" action=mark-connection new-connection-mark=vrrp_wan1_conn
add chain=input in-interface="vrrp$Wan2Vlan" action=mark-connection new-connection-mark=vrrp_wan2_conn
add chain=output connection-mark=vrrp_wan1_conn action=mark-routing new-routing-mark=to_wan1
add chain=output connection-mark=vrrp_wan2_conn action=mark-routing new-routing-mark=to_wan2

# Create marks for incomming connections 
/ip firewall mangle
add chain=prerouting in-interface="vrrp$Wan1Vlan" connection-state=new passthrough=no action=mark-connection new-connection-mark=incomming_connection_wan1
add chain=prerouting in-interface="vrrp$Wan2Vlan" connection-state=new passthrough=no action=mark-connection new-connection-mark=incomming_connection_wan2
add chain=prerouting in-interface="vrrp$LocalVlan" connection-mark=incomming_connection_wan1 passthrough=yes action=mark-routing new-routing-mark=to_wan1
add chain=prerouting in-interface="vrrp$LocalVlan" connection-mark=incomming_connection_wan2 passthrough=yes action=mark-routing new-routing-mark=to_wan2


# VRRP for Wan1
/interface vrrp add interface=wan1 vrid="$Wan1Vlan" priority="100" name="vrrp$Wan1Vlan"
/ip address add address="192.168.$Wan1Vlan.$VrrpIp/32" interface="vrrp$Wan1Vlan"

:if ($VrrpPriority=true) do={
    /interface vrrp set [find name="vrrp$Wan1Vlan"] priority=254
}

# VRRP for Wan2
/interface vrrp add interface=wan2 vrid="$Wan2Vlan" priority="100" name="vrrp$Wan2Vlan"
/ip address add address="192.168.$Wan2Vlan.$VrrpIp/32" interface="vrrp$Wan2Vlan"

:if ($VrrpPriority=true) do={
    /interface vrrp set [find name="vrrp$Wan2Vlan"] priority=254
}

# VRRP for Wan3
/interface vrrp add interface=wan3 vrid="$Wan3Vlan" priority="100" name="vrrp$Wan3Vlan"
/ip address add address="192.168.$Wan3Vlan.$VrrpIp/32" interface="vrrp$Wan3Vlan"

:if ($VrrpPriority=true) do={
    /interface vrrp set [find name="vrrp$Wan3Vlan"] priority=254
}

#/interface list member add list=wans interface="vrrp$Wan1Vlan"
#/interface list member add list=wans interface="vrrp$Wan2Vlan"
#/interface list member add list=wans interface="vrrp$Wan3Vlan"

##########
## DHCP ##
##########

# pool for Local network
/ip pool
add name=dhcp-pool ranges="192.168.$LocalVlan.$DhcpStart-192.168.$LocalVlan.$DhcpEnd"

# Enabling dhcp-server
/ip dhcp-server
add address-pool=dhcp-pool disabled=yes interface=local name=local-dhcp-server

# setup dhcp for Local network
:if ($VrrpPriority=true) do={
    /ip dhcp-server network \
    add address="192.168.$LocalVlan.0/24" dns-server="192.168.$LocalVlan.$VrrpIp,192.168.$LocalVlan.$VrrpIpSec" domain="$Domain" gateway="192.168.$LocalVlan.$VrrpIp"
} else {
    /ip dhcp-server network \
    add address="192.168.$LocalVlan.0/24" dns-server="192.168.$LocalVlan.$VrrpIp,192.168.$LocalVlan.$VrrpIpSec" domain="$Domain" authoritative="after-2sec-delay" gateway="192.168.$LocalVlan.$VrrpIpSec"
}

#########
## DNS ##
#########

/ip dns
set allow-remote-requests=yes cache-size=5000KiB max-udp-packet-size=512 servers="$DnsPri,$DnsSec"

#########
## NAT ##
#########

/ip firewall nat
add action=dst-nat chain=dstnat dst-address="192.168.$Wan1Vlan.$VrrpIp" dst-port=22 in-interface=wan1 protocol=tcp to-addresses="192.168.$LocalVlan.35" to-ports=22
add action=dst-nat chain=dstnat dst-address="192.168.$Wan2Vlan.$VrrpIp" dst-port=22 in-interface=wan2 protocol=tcp to-addresses="192.168.$LocalVlan.35" to-ports=22

###############
## Guest LAN ##
###############

/interface bridge vlan
add bridge=bridge1 vlan-ids="$GuestVlan" tagged=ether4,ether5

/interface vlan
add name=guest vlan-id="$GuestVlan" interface=bridge1 disabled=no

/ip address
add address="192.168.$GuestVlan.$RouterIp/24" network="192.168.$GuestVlan.0" broadcast="192.168.$GuestVlan.255" interface=guest

/interface vrrp add interface=guest vrid="$GuestVlan" priority=254 name="vrrp$GuestVlan"
/ip address add address="192.168.$GuestVlan.$VrrpIp/32" interface="vrrp$GuestVlan"

:if ($VrrpPriority=true) do={
    /interface vrrp set [find name="vrrp$GuestVlan"] priority=100
}

/ip pool
add name=guest-pool ranges="192.168.$GuestVlan.$DhcpStart-192.168.$GuestVlan.$DhcpEnd"

/ip dhcp-server
add address-pool=guest-pool disabled=yes interface=guest name=guest-dhcp-server

/ip dhcp-server network
add address="192.168.$GuestVlan.0/24" dns-server="192.168.$GuestVlan.$VrrpIp" domain="$Domain" gateway="192.168.$GuestVlan.$VrrpIp"


#############
## OpenVPN ##
#############

# Basado en: https://wiki.mikrotik.com/wiki/OpenVPN
# import certificates

/certificate
import file="$Domain.crt"
import file="$Domain.pem"
import file=ca.crt

/ip pool
add name=ovpn-pool ranges="10.$OpenVpnVlan.32.34-10.$OpenVpnVlan.32.38"

/ppp profile
add local-address="10.$OpenVpnVlan.32.33" name=vpn_profile remote-address=ovpn-pool use-encryption=require

/interface ovpn-server server
set certificate="$Domain.crt_0" cipher=blowfish128,aes128,aes192,aes256 default-profile=vpn_profile enabled=\
    yes keepalive-timeout=disabled netmask=29

/ppp secret
add name="$OpenVpnUser" password="$OpenVpnPass"

/ip firewall filter
add action=accept chain=input comment=Openvpn dst-port=1194 protocol=tcp

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# Bridge VLANS VERSION: Enabling Vlan Filtering in bridge1 !!
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

/interface bridge set bridge1 vlan-filtering=yes