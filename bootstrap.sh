#!/bin/bash
## This program must be run with: # bash bootstrap.sh
## RUN WITH BASH and root !!!!

DISTRO=jessie


ARCH=amd64

IPADDRESS=192.168.19.99
NETMASK=255.255.255.0
GATEWAY=192.168.19.1
DNS=192.168.19.1

DOMAIN=fmoragas.com
HOSTNAME=PCARM00

INSTALL_SOURCES=no
INSTALL_CHEF_CLIENT_SRC=no
INSTALL_LIBVIRT=no

USERNAME=kmoragas
SHARED_USERNAME=sherlock

SSH_PUB_KEY="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDF7VAB6ZlArYo87azfClz/HfB7unafxmRh8l6DCVehKVSm9HTPdKnk5IC/+6ZotENEwc7FFtxZJ5ffg8xal8zPkV56cyShn3vPcUoLC/J/fM40Vubtkpawk4BYX/vdYDGPy5L5aT1Eule/n3vtvRqFS5Vn+7wwko3nawqz/gFQzi8U9XmqqjjRfv3jF8cYPkFI2EmYOmIfYkDYiid/oONrQjhKmXNkA7uIhU0qldDMFHOmeCqBusn76vGIGpUiRkHImazfikE3Uo6LRT0CKsDCeGZjzIObsSXo1B25GCDj3J2cpV43sAwMh+5500gsaeiPNQwPtZCqeX5LA6UzRdKH "

IFACE=eth0

####### SETTIN UP VARIABLES

set_variables() {
	
	echo -n "Enter the server's Debian version and press [ENTER] (jessie/stretch): "
	read DISTRO
	
	if [ -z "${DISTRO}" ]; then
	    DISTRO=jessie
	fi
	
	echo -n "Enter the server's PRINCIPAL interface and press [ENTER] ('eth0' by default): "
	read IFACE
	
	if [ -z "${IFACE}" ]; then
	    IFACE=eth0
	fi
	
	echo -n "Enter you username and press [ENTER] ('kmoragas' if blank): "
	read USERNAME
	
	if [ -z "${USERNAME}" ]; then
	    USERNAME=kmoragas
	fi
	
	echo -n "Enter the shared username and press [ENTER] ('sherlock' if blank): "
	read SHARED_USERNAME
	
	if [ -z "${SHARED_USERNAME}" ]; then
	    SHARED_USERNAME=sherlock
	fi
	
	echo -n "Enter the server's HOSTNAME and press [ENTER]: "
	read HOSTNAME
	
	echo -n "Enter the server's DOMAIN and press [ENTER] ('fmoragas.com' if blank): "
	read DOMAIN
	
	if [ -z "${DOMAIN}" ]; then
	    DOMAIN=fmoragas.com
	fi
	
	echo -n "Enter the server's architecture and press [ENTER] (amd64 / arm): "
	read ARCH
	
	echo -n "Enter the server's IP ADDRESS and press [ENTER]: "
	read IPADDRESS
	
	echo -n "Enter the NETMASK and press [ENTER] ('255.255.255.0' if blank): "
	read NETMASK
	
	if [ -z "${NETMASK}" ]; then
	    NETMASK=255.255.255.0
	fi
	
	echo -n "Enter the server's GATEWAY and press [ENTER]: "
	read GATEWAY
	
	echo -n "Enter the server's DNS and press [ENTER]: "
	read DNS
	
	echo -n "Want to setup esslingen.de as repo? (yes/no): "
	read INSTALL_SOURCES
	
	echo -n "Want to setup chef-client from source? (yes/no): "
	read INSTALL_CHEF_CLIENT_SRC
	
	echo -n "Want to setup libvirt (KVM)? (yes/no): "
	read INSTALL_LIBVIRT

}


### Install Esslingen Sources Funcition
install_sources() {
	
	echo "Setting up sources from ESSLINGEN..."
	
		cat > /tmp/sources.list << EOF
deb http://ftp-stud.hs-esslingen.de/debian/ $DISTRO main
deb-src http://ftp-stud.hs-esslingen.de/debian/ $DISTRO main

deb http://security.debian.org/ $DISTRO/updates main
deb-src http://security.debian.org/ $DISTRO/updates main

deb http://ftp-stud.hs-esslingen.de/debian/ $DISTRO-updates main
deb-src http://ftp-stud.hs-esslingen.de/debian/ $DISTRO-updates main
EOF
	
		cp /tmp/sources.list /etc/apt/sources.list
}

####### Install the required packages

install_packages() {
	echo "Installing packages..."
	
	apt-get update 
	apt-get install -y openssh-server ntp sudo telnet unattended-upgrades wget vim screen byobu inetutils-ping cron xinetd 
}

####### Creates the Group sudo no password
	
set_user_group() {

	echo "Adding groups sudonp and ssh_allow"
	
	addgroup sudonp
	
	####### Creates the group for allowing ssh connection
	addgroup ssh_allow
	
	####### ADD USER
	
	echo "Adding user: $USERNAME"
	
	mkdir -p /tmp/home/$USERNAME/.ssh
	echo $SSH_PUB_KEY > /tmp/home/$USERNAME/.ssh/authorized_keys
	
	useradd -s /bin/bash -m -G sudonp,ssh_allow -k /tmp/home/$USERNAME $USERNAME
	
	
	###### Sudo HARDENING #####
	# add sudonp to the sudoers
	
	echo "Setting up basic hardning"
	
	echo "%sudonp ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers
	
	####### SSH HARDENING ######
	# add ssh_allow to the group
	echo "AllowGroups ssh_allow $SHARED_USERNAME" >> /etc/ssh/sshd_config
	
	####### Remove root for loggin
	sed -ie 's/PermitRootLogin yes/PermitRootLogin no/' /etc/ssh/sshd_config

}

####### PERIODIC UPDATES #######
# add the periodic updates to apt

set_periodic_updates() {
	
	cat > /etc/apt/apt.conf.d/02periodic << EOF
// Enable the update/upgrade script (0=disable)
APT::Periodic::Enable "1";

// Do "apt-get update" automatically every n-days (0=disable)
APT::Periodic::Update-Package-Lists "1";

// Do "apt-get upgrade --download-only" every n-days (0=disable)
APT::Periodic::Download-Upgradeable-Packages "1";

// Run the "unattended-upgrade" security upgrade script
// every n-days (0=disabled)
// Requires the package "unattended-upgrades" and will write
// a log in /var/log/unattended-upgrades
APT::Periodic::Unattended-Upgrade "1";

// Do "apt-get autoclean" every n-days (0=disable)
APT::Periodic::AutocleanInterval "7";
EOF

}

###### SET SERVERNAME

set_servername() {
	
	echo "Setting up Server Name..."
	
	sed -ie "s/hostname: .*/hostname: $HOSTNAME/"  /boot/device-init.yaml
	
	hostname $HOSTNAME
	
	echo $HOSTNAME > /etc/hostname
	
	echo "127.0.0.1		$HOSTNAME" >> /etc/hosts

}

####### SET IP ADDRESS

set_network() {

	echo "Setting up Network interface $IFACE..."

    cat > /tmp/$IFACE << EOF
auto lo
iface lo inet loopback

allow-hotplug $IFACE
iface $IFACE inet static
    address $IPADDRESS
    netmask $NETMASK
    gateway $GATEWAY
    dns-nameservers $DNS
    dns-search $DOMAIN
EOF

    cp /tmp/$IFACE /etc/network/interfaces.d/$IFACE
        
}

set_network_bridge() {

	echo "Setting up Network Bridge interface $IFACE..."

    cat > /tmp/$IFACE << EOF
auto lo
iface lo inet loopback

auto br0
iface br0 inet static
      address $IPADDRESS
      netmask $NETMASK
      gateway $GATEWAY
      dns-nameservers $DNS
      dns-search $DOMAIN
      bridge_ports $IFACE
      bridge_stp off
      bridge_maxwait 0
EOF

    cp /tmp/$IFACE /etc/network/interfaces.d/$IFACE
        
}

## Docker
	
install_docker() {

	sudo apt-get install \
	     apt-transport-https \
	     ca-certificates \
	     curl \
	     gnupg2 \
	     software-properties-common

	curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | sudo apt-key add -

	case "$lsb_dist" in
		amd64)
	
			sudo add-apt-repository \
			   "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
			   $(lsb_release -cs) \
			   stable"

			;;
		arm|armhf)

			echo "deb [arch=armhf] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
			     $(lsb_release -cs) stable" | \
			    sudo tee /etc/apt/sources.list.d/docker.list
			;;
			
	esac

	
	sudo apt-get update

	sudo apt-get install docker-ce	
}

install_docker_compose() {

	## Docker Compose

	sudo curl -L https://github.com/docker/compose/releases/download/1.16.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

	sudo chmod +x /usr/local/bin/docker-compose
	
}

install_libvirt() {

	sudo apt install -y qemu-kvm libvirt0 bridge-utils
		
}


####### SETUP CHEF-CLIENT from SRC

install_chef_client_src() {

	echo "Setting up Chef-client from SRC..."

	# taken from: http://blog.ittoby.com/2016/04/installing-chef-on-raspberry-pi-23.html

	apt-get install -y gcc make libssl-dev

	cd /usr/src
	wget https://cache.ruby-lang.org/pub/ruby/2.4/ruby-2.4.2.tar.gz
	tar -xvzf ruby-2.4.2.tar.gz
	cd ruby-2.4.2

	./configure --enable-shared --disable-install-doc --disable-install-rdoc --disable-install-capi

	make -j4 ; make install

	gem install chef
	
}

####### ADD Shared User

add_shared_user() {

	echo "Adding shared User [$SHARED_USERNAME]"
	
	adduser $SHARED_USERNAME

}

####### DELETE PIRATE USER

delete_users() {

	echo "Deleting users: pirate, debian and ubuntu"
	deluser pirate
	rm -rf /home/pirate
	
	deluser debian
	rm -rf /home/debian
	
	deluser ubuntu
	rm -rf /home/ubuntu

}

do_install() {
	
	echo "Welcome to the bootstrap process!!!"
	
	set_variables
	
	echo ""
	echo "****** Here we go!!!! *******"
	echo ""
	
	if [[ $INSTALL_SOURCES == "yes" ]]; then
		install_sources
	fi
	
	install_packages
	
	set_user_group
	
	set_network
	
	set_servername
	
	set_periodic_updates

	
	if [[ "$INSTALL_CHEF_CLIENT_SRC" == "yes" ]]; then
		install_chef_client_src
	fi
	
	if [[ "$INSTALL_DOCKER" == "yes" ]]; then
		install_docker
	fi
	
	if [[ "$INSTALL_DOCKER_COMPOSE" == "yes" ]]; then
		install_docker_compose
	fi
	
	if [[ "$INSTALL_LIBVIRT" == "yes" ]]; then
		install_libvirt
		set_network_bridge
	fi
	
	delete_users
	
	add_shared_user
	
	echo ""
	echo "****** DONE: REBOOTING *******"
	echo ""
	
	wait 5
	
	reboot
	
}

do_install