1. Copy `bootstrap.sh` it will change IP Address, hostname. Also installs byubu, ping, openssh-server. Basic harden (create groups and users), installs chef-client on ARM. 
2. Bootstrap your node with knife
```
knife bootstrap ADDRESS --ssh-user USER --sudo --identity-file IDENTITY_FILE --node-name node1-centos
```

3. Update configuration on your node
```
knife ssh 'name:node1-centos' 'sudo chef-client' --ssh-user USER --identity-file IDENTITY_FILE --attribute ipaddress

```

4. Search some nodes in env production
```
knife search node 'chef_environment:production'
```

5. Turn off all of them
```
knife ssh 'chef_environment:production' "sudo poweroff" --ssh-user USER --identity-file IDENTITY_FILE  -a ipaddress
```
